//
//  AppDelegate.h
//  AESUtilsDemo
//
//  Created by ilongge on 2021/7/29.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

