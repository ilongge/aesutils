//
//  ViewController.m
//  AESUtilsDemo
//
//  Created by ilongge on 2021/7/29.
//
#import "AESUtils+Project.h"
#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *encodeString = @"asdfghjkl";
    NSLog(@"%@", encodeString);
    NSString *base64String = [AESUtils encryptWithText:encodeString andMode:AES_Mode_ECB];
    NSLog(@"%@", base64String);
    base64String = [AESUtils decryptWithText:base64String andMode:AES_Mode_ECB];
    NSLog(@"%@", base64String);
}

@end
