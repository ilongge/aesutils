//
//  AESUtils+Project.h
//  AESUtilsDemo
//
//  Created by ilongge on 2021/12/2.
//

#import "AESUtils.h"

@interface AESUtils (Project)
/**
 * AES加密_Project
 * @param encryptData  需要加密的二进制数据
 * @param mode         加密模式 
 */
+ (NSData *)encryptWithData:(NSData *)encryptData andMode:(AES_Mode)mode;

/**
 * AES加密_Project
 * @param encryptText  需要加密的字符串
 * @param mode         加密模式
 */
+ (NSString *)encryptWithText:(NSString *)encryptText andMode:(AES_Mode)mode;

/**
 * AES解密_Project
 * @param decryptData  需要解密的二进制数据
 * @param mode         加密模式
 */
+ (NSData *)decryptWithData:(NSData *)decryptData andMode:(AES_Mode)mode;

/**
 * AES解密_Project
 * @param decryptText  需要解密的字符串
 * @param mode         加密模式
 */
+ (NSString *)decryptWithText:(NSString *)decryptText andMode:(AES_Mode)mode;
@end


