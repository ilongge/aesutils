//
//  NSData+Add.h
//  AESUtilsDemo
//
//  Created by ilongge on 2022/1/4.
//

#import <Foundation/Foundation.h>

@interface NSData (Add)
- (NSString *)sha1String;

- (NSString *)sha224String;

- (NSString *)sha256String;

- (NSString *)sha384String;

- (NSString *)sha512String;
@end

 
