//
//  AESUtils.h
//  AESUtilsDemo
//
//  Created by ilongge on 2021/7/29.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, AES_Mode){
    AES_Mode_CBC,
    AES_Mode_ECB
};

@interface AESUtils : NSObject

/**
 * AES加密
 * @param encryptKey   加密秘钥
 * @param ivKey        偏移秘钥
 * @param encryptData  需要加密的二进制数据
 */
+ (NSData *)encryptWithKey:(NSString *)encryptKey andIvKey:(NSString *)ivKey andData:(NSData *)encryptData;

/**
 * AES加密
 * @param encryptKey   加密秘钥
 * @param ivKey        偏移秘钥
 * @param encryptText  需要加密的字符串
 */
+ (NSString *)encryptWithKey:(NSString *)encryptKey andIvKey:(NSString *)ivKey andText:(NSString *)encryptText;

/**
 * AES解密
 * @param decryptKey   解密秘钥
 * @param ivKey        偏移秘钥
 * @param decryptData  需要解密的二进制数据
 */
+ (NSData *)decryptWithKey:(NSString *)decryptKey andIvKey:(NSString *)ivKey andData:(NSData *)decryptData;

/**
 * AES解密
 * @param decryptKey   解密秘钥
 * @param ivKey        偏移秘钥
 * @param decryptText  需要解密的字符串
 */
+ (NSString *)decryptWithKey:(NSString *)decryptKey andIvKey:(NSString *)ivKey andText:(NSString *)decryptText;
/**
 * AES加密
 * @param encryptKey   加密秘钥
 * @param ivKey        偏移秘钥
 * @param mode         加密模式
 * @param encryptData  需要加密的二进制数据
 */
+ (NSData *)encryptWithKey:(NSString *)encryptKey andIvKey:(NSString *)ivKey andMode:(AES_Mode)mode andData:(NSData *)encryptData;

/**
 * AES加密
 * @param encryptKey   加密秘钥
 * @param ivKey        偏移秘钥
 * @param mode         加密模式
 * @param encryptText  需要加密的字符串
 */
+ (NSString *)encryptWithKey:(NSString *)encryptKey andIvKey:(NSString *)ivKey andMode:(AES_Mode)mode andText:(NSString *)encryptText;

/**
 * AES解密
 * @param decryptKey   解密秘钥
 * @param ivKey        偏移秘钥
 * @param mode         加密模式
 * @param decryptData  需要解密的二进制数据
 */
+ (NSData *)decryptWithKey:(NSString *)decryptKey andIvKey:(NSString *)ivKey andMode:(AES_Mode)mode andData:(NSData *)decryptData;

/**
 * AES解密
 * @param decryptKey   解密秘钥
 * @param ivKey        偏移秘钥
 * @param mode         加密模式
 * @param decryptText  需要解密的字符串
 */
+ (NSString *)decryptWithKey:(NSString *)decryptKey andIvKey:(NSString *)ivKey andMode:(AES_Mode)mode andText:(NSString *)decryptText;
@end

