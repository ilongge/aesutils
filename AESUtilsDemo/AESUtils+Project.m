//
//  AESUtils+Project.m
//  AESUtilsDemo
//
//  Created by ilongge on 2021/12/2.
//

#define EncryptKey  @"pcs**1"
#define IvKey       @"pcs**1"

#import "AESUtils+Project.h"

@implementation AESUtils (Project)

+ (NSData *)encryptWithData:(NSData *)encryptData andMode:(AES_Mode)mode
{
    return [AESUtils encryptWithKey:EncryptKey andIvKey:IvKey andMode:mode andData:encryptData];
}

+ (NSString *)encryptWithText:(NSString *)encryptText andMode:(AES_Mode)mode
{
    NSData *encryptTextData = [encryptText dataUsingEncoding:NSUTF8StringEncoding];
    NSData *encryptData = [AESUtils encryptWithData:encryptTextData andMode:mode];
    NSString *base64String = [encryptData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    return base64String;
}

+ (NSData *)decryptWithData:(NSData *)decryptData andMode:(AES_Mode)mode
{
    return [AESUtils decryptWithKey:EncryptKey andIvKey:IvKey andMode:mode andData:decryptData];
}

+ (NSString *)decryptWithText:(NSString *)decryptText andMode:(AES_Mode)mode
{
    NSData * base64Data = [[NSData alloc] initWithBase64EncodedString:decryptText options:NSDataBase64DecodingIgnoreUnknownCharacters];
    base64Data = [AESUtils decryptWithData:base64Data andMode:mode];
    NSString *base64String = [[NSString alloc] initWithData:base64Data encoding:NSUTF8StringEncoding];
    return  base64String;
}
@end
